# Subscale

Implementation of the Subscale algorithm in Python.

- To run the algorithm for profiling the files
- run_python.py or run_cpp.py are required to be executed.
- Usage: python run_python.py 5
- The argument defines the number of maximal min_point value starting from min_points=2
- The files in data_sets.txt are used for the selection of data sets in the data folder.
