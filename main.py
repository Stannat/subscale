import os
import sys
import getopt

import modules.IO as DB
import modules.SUBSCALE as SUBSCALE
import modules.Timer as tm
import modules.HelperFunctions as HF

argument_list = sys.argv[1:]
short_options = ["js"]
long_options = ["json="]


def single_core():
    config = None
    try:
        opts, values = getopt.getopt(argument_list, short_options, long_options)
    except getopt.error as err:
        # Output error, and return with an error code
        print(str(err))
        sys.exit(2)
    json = [j for j in opts if "--json" in j and "None" not in j]
    assert len(json) > 0, "Config file not loaded!\n"

    config = DB.load_json(config_path=json[0][1])
    min_points = config['min_points']
    eps = config['eps']
    filepath = config['dataPath']
    result_path = config['resultPath']
    transposed = config['transposed']
    is_pivot = config['is_pivot']
    split_fac = config['split_fac']
    use_dbscan = config['use_dbscan']
    dbscan_only = config['dbscan_only']
    just_calculate = config['just_calculate']
    is_multiprocessing = config['is_multiprocessing']
    save_slices = config['save_slices']
    is_whole_data_set_loaded = config['is_whole_data_set_loaded']

    # general configuration part
    _database = DB.DB(filepath, result_path, is_transposed=transposed)
    if is_whole_data_set_loaded:
        _database.load_whole_data_set(filepath)
    _database.delete_directory(_database.get_file_path_results())

    # in case if only DBSCAN should be executed.
    # requires subspace files in the result directory to exist
    if dbscan_only:
        HF.run_DBSCAN_only(_database, eps, min_points, full_dimensional=False)
        return

    # subscale specific configuration
    SUBSCALE.init(eps=eps, min_points=min_points, is_pivot=is_pivot, n_processes=1, use_evenly_distributed_slices=False,
                  just_calculate=just_calculate, split_fac=split_fac, is_multiprocessing=is_multiprocessing,
                  save_slices=save_slices)

    # timer configuration
    timer_info = os.path.join(os.path.abspath(_database.get_file_path_results()), "timers.txt")
    tm.init(timer_info)
    tm.saved_timestamps += "Data set:\n{}\n min_points={} eps={} pivot={}".format(filepath.split('/')[-1],
                                                                              SUBSCALE._min_points, SUBSCALE._eps,
                                                                              SUBSCALE._is_pivot)
    print(tm.saved_timestamps)
    tm.start(0)

    # label all points and put them in DB
    tm.start(1)
    SUBSCALE.set_labels(_database)
    tm.time_display(nr=1, msg="All points labeled")

    # calculation of min and max signatures
    tm.start(1)
    SUBSCALE._minSig, SUBSCALE._maxSig = SUBSCALE.calc_min_max_signatures(SUBSCALE._l_labels, SUBSCALE._min_points)

    tm.time_display(nr=1, msg="Min/max signatures calculated")
    sum_dus = 0  # sum of dense units

    # add 1 to pascal size, since the longest CS could be of rows size
    HF.PascalTriangle.size = _database.get_size_rows() + 1
    # calculate Pascal triangle
    tm.start(1)
    HF.PascalTriangle.create_pascal_triangle(min_points=SUBSCALE._min_points,
                                             num_of_all_points=HF.PascalTriangle.size)
    tm.time_display(nr=1, msg="Pascal triangle calculated", len_of_list=HF.PascalTriangle.size)

    # make dims_at_once dependent on number of launched calculation processes
    dims_at_once = SUBSCALE.calculate_dimensions_at_once(_database)

    # Slices, boundary estimation
    SUBSCALE._l_boundaries = SUBSCALE.slice_boundaries_calculation(io=_database)

    if is_pivot:
        SUBSCALE.partition_pivot_execution(dims_at_once=dims_at_once, n_processes=1, io=_database, just_calculate=False)
    else:
        tm.start(1)
        all_core_sets_as_indices = SUBSCALE.create_all_core_sets(_database, SUBSCALE._eps, SUBSCALE._min_points)
        tm.time_display(nr=1, msg="All core sets calculated")

        tm.start(1)
        SUBSCALE.partition_non_pivot_execution(n_processes=1, dims_at_once=dims_at_once,
                                               all_core_sets_as_indices=all_core_sets_as_indices,
                                               labels=SUBSCALE._l_labels, io=_database)
        tm.time_display(nr=3, msg="Signatures added", len_of_list=sum_dus, dont_set_end_time=True)

    if SUBSCALE._has_slices:
        SUBSCALE.refine_subspaces(io=_database)
    else:
        tm.start(1)
        SUBSCALE.SubscaleDict.add_to_dict_same_subspaces()
        tm.time_display(nr=1, msg="Subspaces-Table created", len_of_list=len(SUBSCALE.SubscaleDict.subspaces))

        tm.start(1)
        _database.save_subspaces_to_csv(SUBSCALE.SubscaleDict.subspaces, is_append=True)
        tm.time_display(nr=1, msg="Subspaces saved")

    # debug
    # how to remove 1 dimension sized subspaces
    # one_element_sized_key = next(iter(clusters))
    # if len(one_element_sized_key) == 1:
    #    clusters.pop(one_element_sized_key)
    if SUBSCALE._n_processes == 1:
        tm.time_display(nr=0, msg="Single Core Algorithm finished")
    else:  # not implemented yet
        tm.time_display(nr=0, msg="Multi Core Algorithm finished")

    if use_dbscan:
        tm.start(0)
        HF.run_DBSCAN(_database, clusters=SUBSCALE.SubscaleDict.subspaces, eps=eps, min_points=min_points,
                      full_dimensional_input=False, full_dimensional_output=True)
        tm.time_display(nr=0, msg="DBSCAN finished")
    tm.out()


def main():
    single_core()


if __name__ == "__main__":
    # Tst.unittest.main()
    # main_cython.main()
    main()
