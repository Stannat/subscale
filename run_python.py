import json
import pathlib
import subprocess
import os
import sys
import shutil

import numpy
import pandas
import sklearn

python_file = 'main.py'
config_path = 'config.json'
data_files = 'data_sets.txt'
modules = {}


def change_dir(new_working_dir):
    if os.path.basename(os.path.normpath(os.getcwd())) not in new_working_dir:
        os.chdir(new_working_dir)


def load_filenames(filename):
    l_filenames = list()
    with open(filename, 'r') as data_file:
        for line in data_file:
            if len(line) > 1:
                l_filenames.append(line.strip('\n'))
    return l_filenames


def load_json(config, config_path):
    abs_config_path = os.path.abspath(config_path)
    with open(abs_config_path) as json_file:
        config = json.load(json_file)
    return config


def update_json(config, config_path):
    abs_config_path = os.path.abspath(config_path)
    with open(abs_config_path, "w") as json_file:
        json.dump(config, json_file)


def create_result_path(directory):
    res_path = pathlib.Path(directory)
    if not os.path.exists(res_path):
        os.makedirs(res_path)


def run_SUBSCALE(data_files, max_min_points=8):
    cfg = None
    cfg = load_json(cfg, config_path)
    count = 0
    _m_min_pts = max_min_points
    for d_f in data_files:
        cfg['dataPath'] = os.path.abspath("./data/{}".format(d_f))
        for min_points in range(2, _m_min_pts + 1):
            print("min_point: {} of {}".format(min_points, _m_min_pts))
            cfg['min_points'] = min_points
            res_subfolder = "./results/Python/{}_{}".format(d_f.split('.')[:-1][0], min_points)
            cfg['resultPath'] = os.path.abspath(res_subfolder)
            create_result_path(cfg['resultPath'])
            update_json(cfg, config_path)
            # cmd = ['start cmd /k ', os.path.abspath(python_file), ' --json=', os.path.abspath(config_path)]
            cmd = 'python main.py --json=config.json'
            p = subprocess.Popen(cmd, shell=True)
            p.wait()
            shutil.copy(src=os.path.abspath(config_path), dst=cfg['resultPath'] + '/')
            count += 1
            print()
        _m_min_pts -= 1
    print("Amount of executions: {}".format(count))


def make_import(module):
    return __import__(module)


def check_libraries():
    try:
        arr = numpy.array([1, 2, 3, 4, 5])
    except Exception:
        print("No NumPy installed")
        if input("import NumPy?").strip() == "y":
            numpy = make_import("numpy")

    try:
        arr = pandas.DataFrame()
    except Exception:
        print("No Pandas installed")
        if input("import pandas?").strip() == "y":
            pandas = make_import("pandas")

    try:
        sklearn.datasets.load_iris()
    except Exception:
        print("No sklearn installed")
        if input("import sklearn?").strip() == "y":
            sklearn = make_import("sklearn")


def main():
    global data_files
    # change_dir(r'./')

    #check_libraries()

    data_files = load_filenames(data_files)
    if len(sys.argv) > 1:
        run_SUBSCALE(data_files, int(sys.argv[1]))
    else:
        run_SUBSCALE(data_files)


if __name__ == "__main__":
    main()
