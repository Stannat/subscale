import json
import pathlib
import subprocess
import os
import sys
import shutil

import numpy
import pandas
import sklearn

cpp_file = './SUBSCALE_C++/SubscaleGPU.exe'
config_path = './SUBSCALE_C++/config.json'
data_files = 'data_sets.txt'


def change_dir(new_working_dir):
    if os.path.basename(os.path.normpath(os.getcwd())) not in new_working_dir:
        os.chdir(new_working_dir)


def load_filenames(filename):
    l_filenames = list()
    with open(filename, 'r') as data_file:
        for line in data_file:
            if len(line) > 1:
                l_filenames.append(line.strip('\n'))
    return l_filenames


def load_json(config, config_path):
    abs_config_path = os.path.abspath(config_path)
    with open(abs_config_path) as json_file:
        config = json.load(json_file)
    return config


def update_json(config, config_path):
    abs_config_path = os.path.abspath(config_path)
    with open(abs_config_path, "w") as json_file:
        json.dump(config, json_file)


def create_result_path(directory):
    res_path = pathlib.Path(directory)
    if not os.path.exists(res_path):
        os.makedirs(res_path)


def run_SUBSCALE(data_files, max_min_points=8):
    cfg = None
    cfg = load_json(cfg, config_path)
    count = 0
    _m_min_pts = max_min_points
    for d_f in data_files:
        cfg['dataPath'] = os.path.abspath("./data/{}".format(d_f))
        cfg['dataPath'] = cfg['dataPath'].replace(os.sep, '/')
        for min_points in range(2, _m_min_pts + 1):
            print("min_point: {} of {}".format(min_points, _m_min_pts))
            cfg['minPoints'] = min_points
            for i in range(2):
                if i % 2 == 0:
                    cfg['runSequential'] = True
                    is_run_sequential = "run_seq"
                else:
                    cfg['runSequential'] = False
                    is_run_sequential = "run_par"
                res_subfolder = "./results/Cpp/{}/{}_{}/".format(is_run_sequential, d_f.split('.')[:-1][0], min_points)
                cfg['resultPath'] = os.path.abspath(res_subfolder)
                cfg['resultPath'] = cfg['resultPath'].replace(os.sep, '/') +'/'
                create_result_path(cfg['resultPath'])
                update_json(cfg, config_path)
                print(is_run_sequential)
                cmd = [os.path.abspath(cpp_file), os.path.abspath(config_path)]
                p = subprocess.Popen(cmd, shell=True)
                p.wait()
                count += 1
                print()
        _m_min_pts -= 1
    print("Amount of executions: {}".format(count))


def main():
    global data_files
    # change_dir(r'./')

    data_files = load_filenames(data_files)
    if len(sys.argv) > 1:
        run_SUBSCALE(data_files, int(sys.argv[1]))
    else:
        run_SUBSCALE(data_files)


if __name__ == "__main__":
    main()
