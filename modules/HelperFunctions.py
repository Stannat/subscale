import numpy as np
from sklearn.cluster import DBSCAN
import modules.IO as io
from modules import SUBSCALE
import modules.Timer as tm

# The p-exponent, for Minkovsky distance, change it to your needs.
_p = 4


def run_DBSCAN(db, clusters, eps, min_points,
               full_dimensional_input=False, full_dimensional_output=True):
    known_points = dict()
    candidates = dict()
    maximal_clusters_stat = dict()
    total_points = 0
    for subspace, points in clusters.items():
        total_points += len(points)
        for point in points:
            if point not in known_points:
                required_points = [p[1] for p in db.get_point_with_pos(point)]
                known_points[point] = required_points
            if subspace in candidates:
                candidates[subspace] += [known_points[point]]
            else:
                candidates[subspace] = [known_points[point]]
    # remove old files
    db.dbscan_remove()
    subspace_col = dict()
    subspace_last_len = -1
    tm.start(1)
    subspace = list()
    for subspace, candidate_points in candidates.items():
        # tm.start(1)
        if full_dimensional_input:
            candidate_points_in_dim = np.array(candidate_points)
        else:
            candidate_points_in_dim = np.array(candidate_points)[:, subspace]

        clusters = DBSCAN(eps=eps, min_samples=min_points).fit(candidate_points_in_dim)
        # tm.time_display(nr=1, msg="Subspace={}".format(list(subspace)))
        # count number of clusters in subspace
        # tm.start(1)
        DBSCAN_subspace_cluster_size(clusters.labels_, maximal_clusters_stat, subspace)
        # tm.time_display(nr=1, msg="Clusters={}".format(list(subspace)))
        if len(subspace) != subspace_last_len:
            tm.time_display(nr=1, msg="Calculated={}".format(len(list(subspace))))
            tm.start(1)
            db.dbscan_save_all(subspace_col)
            tm.time_display(nr=1, msg="Saved={}".format(len(list(subspace))))
            subspace_last_len = len(subspace)
            subspace_col = dict()
            tm.start(1)
        if not full_dimensional_output and not full_dimensional_input:
            subspace_col[subspace] = [candidate_points_in_dim, clusters.labels_]
        else:
            subspace_col[subspace] = [candidate_points, clusters.labels_]
    tm.time_display(nr=1, msg="Calculated={}".format(len(list(subspace))))
    tm.start(1)
    db.dbscan_save_all(subspace_col)
    tm.time_display(nr=1, msg="Saved={}".format(len(list(subspace))))
    db.dbscan_count_out(maximal_clusters_stat)
    return total_points


def minkowsky(x, y):
    """
    Calculates the minkowsky distance between two values
    :param x: first point as vector
    :param y: second point as vector
    :return: skalar value as the distance between both points
    """
    # the higher the p-exponent, the smaller is the result
    subbed = np.subtract(x, y)
    powered = np.power(subbed, _p)
    summed = np.sum(powered)
    return summed ** (1 / _p)


def run_DBSCAN_only(db, eps, min_points, full_dimensional=True):
    tm.start(0)
    res = db.load_all_subspaces(SUBSCALE.SubscaleDict.subspaces)
    tm.time_display(nr=0, msg="DBSCAN input loaded")
    if res:
        tm.start(0)
        total_points = run_DBSCAN(db, SUBSCALE.SubscaleDict.subspaces, eps, min_points, full_dimensional)
        tm.time_display(nr=0, msg="DBSCAN finished. Totalpoints={}".format(total_points))
    tm.out()


def DBSCAN_subspace_cluster_size(labels, maximal_clusters_stat, subspace):
    """
    Counts the size of clusters (how many points per cluster) for each subspace
    :param labels: result from DBSCAN
    :param maximal_clusters_stat: dictionary of subspaces
    :param subspace: the subspace for the counting
    :return: updated maximal_clusters_stat
    """
    for label in labels:
        if subspace not in maximal_clusters_stat:
            maximal_clusters_stat[subspace] = dict({label: 1})
        else:
            if label in maximal_clusters_stat[subspace]:
                maximal_clusters_stat[subspace][label] += 1
            else:
                maximal_clusters_stat[subspace][label] = 1
    return maximal_clusters_stat


class PascalTriangle:
    __pascal_triangle = None
    __min_points = None
    __num_of_all_points = None
    size = 0

    def __init__(self, min_points, num_of_all_points):
        PascalTriangle.__pascal_triangle = [0] * min_points
        PascalTriangle.__pascal_triangle[0] = list(range(num_of_all_points))
        PascalTriangle.__min_points = min_points
        PascalTriangle.__num_of_all_points = num_of_all_points
        PascalTriangle.calculate_pascal_triangle()

    @staticmethod
    def calculate_pascal_triangle():
        """
        Calculates min_point libs in the pascal triangle
        Each lib has the size of  num_of_all_points
        :return: None
        """
        for min_point in range(2, PascalTriangle.__min_points + 1):
            list_nr = [0]
            PascalTriangle.__pascal_triangle[min_point - 1] = list_nr
            for point in range(1, PascalTriangle.__num_of_all_points):
                next_number = list_nr[point - 1] + \
                              PascalTriangle.__pascal_triangle[min_point - 2][point - 1]
                list_nr.append(next_number)

    @staticmethod
    def create_pascal_triangle(min_points, num_of_all_points):
        if not PascalTriangle.__pascal_triangle:
            PascalTriangle(min_points, num_of_all_points)
        return PascalTriangle.__pascal_triangle

    @staticmethod
    def get_pascal_triangle():
        return PascalTriangle.__pascal_triangle
