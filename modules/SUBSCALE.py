import math
from enum import Enum
import modules.IO as DB
import random
from threading import RLock
import modules.FourthDegreeFunction as fdf
import modules.Timer as tm
from modules import HelperFunctions
import multiprocessing


class TypeOfEl(Enum):
    id = 0
    value = 1


_min_points = 0
_eps= 0.0
_is_pivot = True
_split_fac = 1
_minSig = 0  # default value
_maxSig = 1  # default value
_has_slices = False
_use_evenly_distributed_slices = False  # not implemented yet, so keep it to false
_n_processes = 0
_l_boundaries = list()
_just_calculate = False
_is_multiprocessing = False
_l_labels = list()
_is_saved_as_slices = False


# n_processes: more than one processes is not used yet
# use_evenly_distributed_slices: not implemented yet
def init(eps, min_points, is_pivot, n_processes, use_evenly_distributed_slices, just_calculate, split_fac,
         is_multiprocessing=False, save_slices=False):
    global _eps, _min_points, _is_pivot, _n_processes, _use_evenly_distributed_slices, _has_slices, _split_fac
    global _just_calculate, _is_multiprocessing, _is_saved_as_slices
    _eps = eps
    _min_points = min_points
    _n_processes = n_processes  # 1: more than one processes is not used yet
    _use_evenly_distributed_slices = use_evenly_distributed_slices
    _has_slices = split_fac > 1 or _use_evenly_distributed_slices
    _just_calculate = just_calculate
    _is_multiprocessing = is_multiprocessing
    _is_pivot = is_pivot
    _is_saved_as_slices = save_slices
    _split_fac = split_fac


def set_labels(io):
    global _l_labels

    # for cols
    if io.is_transposed():
        _l_labels = label_points(len(io.get_current_row()))
    else:
        # for rows, get from all rows only first column: value = 0 means first column
        _l_labels = label_points(len(io.get_next_dim(start_from_point=0, dim=0)))


def create_core_sets_per_dimension_v2(points, eps, min_points):
    """ Creates CoreSet.

            Parameters
            ----------
            points : list((int, float))
                The list containing projection of all points on the specified dimension.

            eps : float
                The maximum distance that a dense neighbour can be distant to another one

            min_points : int
                The number of minimum neighbours that have to exist in a core set
            """
    assert points
    assert eps > 0.0
    # requirement for min_points
    assert min_points >= 2

    # sort by value (second position in entry)
    points = sorted(points, key=lambda entry: entry[1])
    max_points = len(points)
    l_all_cs = list()
    last_point_id = -1
    next_point = 0
    l_local_cs = list()
    cur_point = 0
    while cur_point < (max_points - 1):
        while next_point < max_points and (points[next_point][1] - points[cur_point][1] <= eps):
            l_local_cs.append(points[next_point][0])
            next_point += 1
        # stop further iterations if the last point is reached for the first time
        if next_point == max_points:
            cur_point = max_points
        else:
            cur_point += 1
        # check if first element from l_local_cs can be removed to prevent index out of range exception
        if len(l_local_cs) > 0:
            # check if id from last element in the current CS is the same as the one in last CS
            if len(l_local_cs) >= min_points and last_point_id != l_local_cs[-1]:
                l_all_cs.append(l_local_cs)
                # save the id of the last point of current CS
                last_point_id = l_local_cs[-1]
            l_local_cs = l_local_cs[1:]
    return l_all_cs



def create_core_sets_per_dimension_v1(points, eps, min_points, type_of_el=TypeOfEl.id):
    """ Creates CoreSet.

            Parameters
            ----------
            points : list((int, float))
                The list containing projection of all points on the specified dimension.

            eps : float
                The maximum distance that a dense neighbour can be distant to another one

            min_points : int
                The number of minimum neighbours that have to exist in a core set

            type_of_el : TypeOfEl, optional
                Enum that describes if the first or second position (int or float) of a list-element
                from l_dim_vec is chosen.
                Default to 0
            """
    assert points
    assert eps > 0.0
    # requirement for min_points
    assert min_points >= 2
    # Type checking
    assert isinstance(type_of_el, TypeOfEl)
    type_of_el = type_of_el.value

    # sort by value (second position in entry)
    points = sorted(points, key=lambda entry: entry[1])
    l_size = len(points)
    l_all_cs = list()
    pos_last_point = -1
    l_local_cs = list()
    for pos1 in range(l_size):
        # recycling of the list from the previous iteration
        if len(l_local_cs) > 0:
            inner_start_pos = pos1 + len(l_local_cs)
            l_local_cs = l_local_cs[1:]
        else:
            inner_start_pos = pos1 + 1
            l_local_cs = list()
        # inner_start_pos = pos1 + 1
        for pos2 in range(inner_start_pos, l_size):  # pos1 < pos2 < l_size

            # abort further execution if pos2 is at the the last point of the dimension
            if pos2 == l_size:
                pos1 = l_size
            # Neighbourhood creation
            dif = points[pos2][1] - points[pos1][1]
            if dif <= eps:
                l_local_cs.append(points[pos2][type_of_el])
            else:  # any next element will be > eps, so get the next point-projection
                pos2 = l_size

        # First condition: only CoreSets of size >= min_points are to be added to the dense unit.
        # min_points-1 because the reference point from which the subtraction is performed
        # has to be added to the list as well.
        # Second condition: CoreSets that are contained in already known CoreSets have to be ignored.
        if (len(l_local_cs) >= min_points - 1) and (pos_last_point != l_local_cs[-1]):
            pos_last_point = l_local_cs[-1]
            # add reference point to CS
            l_local_cs = [points[pos1][type_of_el]] + l_local_cs
            l_all_cs.append(l_local_cs)

    return l_all_cs



def create_all_core_sets(db, eps, min_points):
    """
    Calculation of Core Sets as indices from every dimension
    :param db: DB object
    :param eps: float
        maximal distance between to points that belong together
    :param min_points: int
        number of points that are agglomerated to a cluster
    :param is_transposed: bool
        whether data set is transposed or not
    :return: list[list[list[int]]]
        list of dimensions
        each dimension contains a list of Core Sets
        each Core Set contains point ids
    """
    assert isinstance(db, DB.DB)
    assert eps > 0.0
    assert min_points >= 2

    l_core_sets = list()

    for dim in range(db.get_dimensions()):
        l_pts_in_dim = db.get_next_dim(dim=dim, start_from_point=0)
        cs = create_core_sets_per_dimension_v2(l_pts_in_dim, eps, min_points)
        l_core_sets.append(cs)
    return l_core_sets



def label_points(l_size):
    assert l_size

    l_labels = []
    min_val = 10000000000000
    max_val = 100000000000000
    step = int((max_val - min_val) / l_size)
    for i in range(l_size):
        start_val = min_val + step * i
        end_val = start_val + step
        l_labels.append(random.randint(start_val, end_val))
        # l_labels.append(random.randint(min_val, max_val))
    return l_labels


def calc_min_max_signatures(labels, min_points):
    assert labels
    assert min_points

    size_labels = len(labels)
    assert size_labels > 0
    assert min_points >= 2

    l_min = list()
    l_max = list()

    # initialize for the amount of min_points in both lists
    for i in range(min_points):
        l_min.append(labels[i])
        l_max.append(labels[i])
    l_min.sort()
    l_max.sort()

    # add new minimum and maximum values to the amount of min_points labels
    for i in range(min_points, size_labels):
        if labels[i] < l_min[-1]:
            l_min[-1] = labels[i]
            l_min.sort()
        if labels[i] > l_max[0]:
            l_max[0] = labels[i]
            l_max.sort()

    return sum(l_min), sum(l_max)



def init_seed(c, r):
    """
        Initialization of a seed for lexicographic ordering
        :param c: int
        size of core set
        :param r: int
        size of dense unit / number of minpoints + 1
        :return: [int]
        Initialized seed
    """
    assert c
    assert r

    seed = [c - r + i for i in range(r)]
    seed[0] = -1
    return seed



def create_subsets_lex(cs, r):
    assert cs
    assert r

    dense_units = []
    c = len(cs)
    seed = init_seed(c, r)
    U = [None] * r

    while True:
        i = r - 1
        # array element must equal the calculated index
        # compare from the
        while i >= 0 and seed[i] == c - r + i:
            i -= 1
        if i == -1:
            break
        else:
            temp = seed[i]
            for j in range(i, r):
                k = temp + 1 + j - i
                seed[j] = k
                U[j] = cs[k]
            dense_units.append(list(U))
    return dense_units


def subsequent_colex_index_combination(C, k, last_pos=-1):
    """
    Original algorithm by Nicolas Kiefer
    Calculates the next ordered sequence in a co-lexicographic order.
    The given list has a form of [c_1, c_2, ... c_k] where c_1 > c_2 > ... > c_k
    :param C: the sequence that has to be raised
    :param k: number of elements in sequence
    :param last_pos: the position at which in C the number was raised in the last calculation
    :return: next sequence
    """
    assert C
    assert k
    assert last_pos is not None

    if last_pos != -1:
        i = min(last_pos + 1, k - 1)
    else:
        i = k - 1
    # when the value of the left number == right number - 1
    # then reset the left number to k,...,2,1 dependent on current position
    while i > 0 and C[i] == C[i - 1] - 1:
        C[i] = k - i
        i -= 1
    # increment the number which next neighbour's value is > 1
    C[i] = C[i] + 1
    return C, i



def subsequent_colex_index_combination_2(C, k, last_pos=-1):
    """
        Modified algorithm of Nicolas Kiefer
        Calculates the next ordered sequence in a co-lexicographic order.
        The given list has a form of [c_1, c_2, ... c_k] where c_1 < c_2 < ... < c_k
        :param C: the sequence that has to be raised
        :param k: number of elements in sequence
        :param last_pos: the position at which in C the number was raised in the last calculation
        :return: next sequence
    """
    assert C
    assert k
    assert last_pos is not None

    if last_pos > 1:
        i = k - last_pos + 1
    else:
        i = k
    # when the value of the left number + 1 == right number
    # then reset the left number to one of 1,2,...,k dependent on current position
    while i > 1 and C[k - i] + 1 == C[k - i + 1]:
        C[k - i] = k - i
        i -= 1
    # increment the number which next neighbour's value is > 1
    C[k - i] = C[k - i] + 1
    return C, k - i


def create_dense_units_colexicographic(cs, r, pascal_tri):
    assert cs
    assert r
    assert pascal_tri
    next_seq = [i for i in range(0, r)]
    # next_seq = cs[:r]
    # seed.sort(reverse=True)
    # add initial combination to subsets
    index_combinations = [list(next_seq)]
    last_pos = -1

    # subtract from N k-element sized subsets
    num_of_combinations = pascal_tri[r - 1][len(cs)]
    # create n-1 successive combinations
    for i in range(num_of_combinations - 1):
        next_seq, last_pos = subsequent_colex_index_combination_2(next_seq, r, last_pos)
        index_combinations.append(list(next_seq))

    subsets = list()
    for c in index_combinations:
        combination = list()
        for i in c:
            combination.append(cs[i])
        subsets.append(combination)
    return subsets


def create_dense_units_colexicographic_as_list(cs, r, pascal_tri):
    assert cs
    assert r
    assert pascal_tri
    next_seq = [i for i in range(0, r)]

    # add initial combination to subsets
    subsets = list()
    last_pos = -1

    # add first subset to subsets
    combination = list()
    for j in next_seq:
        combination.append(cs[j])
    subsets.append(combination)

    # subtract from N the k-element sized subsets
    num_of_combinations = pascal_tri[r - 1][len(cs)]
    # create n-1 successive combinations
    for i in range(num_of_combinations - 1):
        next_seq, last_pos = subsequent_colex_index_combination_2(next_seq, r, last_pos)
        combination = list()
        for j in next_seq:
            combination.append(cs[j])
        subsets.append(combination)
    return subsets



def create_dense_units_colexicographic_no_list2(cs, r, next_seq, last_pos=-1):
    assert cs
    assert len(cs) > 0
    assert r

    combination = list()
    for j in next_seq:
        combination.append(cs[j])
    next_seq, last_pos = subsequent_colex_index_combination_2(next_seq, r, last_pos)
    return combination, next_seq, last_pos


def createDU_and_sig_map_no_pivot(start_dim, n_threads, core_sets, labels, min_points, pascal_tri, boundary_low,
                                  boundary_high, just_calculate):
    # calculate start and end dimension
    start = start_dim * n_threads
    end = start + n_threads

    sum_all_calculated_DUs = 0
    total_collisions = 0
    # tm.start(2)
    for dim_pos in range(start, end):
        # for statistics
        # num_cs = len(core_sets[dim_pos])
        # num_cs_elements = sum(len(cs) for cs in core_sets[dim_pos])

        # tm.start(3)
        for cs in core_sets[dim_pos]:
            # cs.sort()
            # tm.start(4)

            # calculate all DUs from a given CS
            tm.time_add(nr=2)
            dus = create_dense_units_colexicographic_as_list(cs, min_points, pascal_tri)
            tm.time_add(nr=2)
            # tm.time_display(4, "Created DUs with Colex-Order for", pos=dim_pos, len_of_list=len(dus))
            # sum_all_calculated_DUs += len(dus)
            # added_DUs_per_cs = 0

            # check for each DU if it is in boundary
            # add to dict if true
            # tm.start(4)
            if not just_calculate:
                for i in range(len(dus)):
                    sig = sum(labels[x] for x in dus[i])
                    tm.time_add(nr=3)
                    if boundary_low <= sig < boundary_high:
                        is_sig_added, num_collisions = SubscaleDict.add_to_dict_same_signatures2(
                            dense_unit_as_indices=dus[i],
                            dimension=dim_pos, signature=sig)
                        # added_DUs_per_cs += int(is_sig_added)
                        # added_DUs_per_cs += num_collisions
                        total_collisions += num_collisions
            # tm.time_display(4, "Added DUs to Signatures-map", pos=dim_pos, len_of_list=added_DUs_per_cs)

        # tm.time_display(3, "Created and added ALL DUs to map in", pos=dim_pos, len_of_list=num_cs)

    # tm.time_display(2, "Boundary {}-{} finished, added DUs".format(boundary_low, boundary_high), len_of_list=total_collisions)
    return total_collisions



def createDU_and_sig_map_no_pivot2(start_dim, n_threads, core_sets, labels, min_points, pascal_tri, boundary_low,
                                   boundary_high, just_calculate):
    # calculate start and end dimension
    start = start_dim * n_threads
    end = start + n_threads

    # for profiling
    sum_cs_pruned = 0

    sum_all_calculated_DUs = 0
    total_collisions = 0
    # tm.start(2)
    for dim_pos in range(start, end):
        # for statistics
        # num_cs = len(core_sets[dim_pos])
        # num_cs_elements = sum(len(cs) for cs in core_sets[dim_pos])

        # tm.start(3)
        for cs in core_sets[dim_pos]:
            # cs.sort()
            # tm.start(4)

            # calculate all DUs from a given CS
            tm.time_add(nr=2)
            new_cs = prune_cs(cs, boundary_low, boundary_high)
            sum_cs_pruned += len(new_cs)
            dus = create_dense_units_colexicographic_as_list(new_cs, min_points, pascal_tri)
            # dus2 = create_dense_units_colexicographic_as_list(cs, min_points, pascal_tri)
            # dus = create_dense_units_colexicographic_as_list(cs, min_points, pascal_tri)
            tm.time_add(nr=2)
            # tm.time_display(4, "Created DUs with Colex-Order for", pos=dim_pos, len_of_list=len(dus))
            # sum_all_calculated_DUs += len(dus)
            # added_DUs_per_cs = 0

            # check for each DU if it is in boundary
            # add to dict if true
            # tm.start(4)
            if not just_calculate:
                for i in range(len(dus)):
                    sig = sum(labels[x] for x in dus[i])
                    tm.time_add(nr=3)
                    if boundary_low <= sig < boundary_high:
                        is_sig_added, num_collisions = SubscaleDict.add_to_dict_same_signatures2(
                            dense_unit_as_indices=dus[i],
                            dimension=dim_pos, signature=sig)
                        # added_DUs_per_cs += int(is_sig_added)
                        # added_DUs_per_cs += num_collisions
                        total_collisions += num_collisions
                """
                for i in range(len(dus2)):
                    sig2 = sum(labels[x] for x in dus2[i])
                    if boundary_low <= sig2 < boundary_high:
                        a = 0
                    tm.time_add(nr=3)
                """
            # tm.time_display(4, "Added DUs to Signatures-map", pos=dim_pos, len_of_list=added_DUs_per_cs)

        # tm.time_display(3, "Created and added ALL DUs to map in", pos=dim_pos, len_of_list=num_cs)

    # tm.time_display(2, "Boundary {}-{} finished, added DUs".format(boundary_low, boundary_high), len_of_list=total_collisions)
    return sum_cs_pruned



def pivot_creation(db, dim, start, dims_at_once,
                   pascal_tri, boundary_low, boundary_high,
                   just_calculate=False, has_slices=False, prune_core_sets=False):
    # abort program if invalid arguments
    assert db
    assert dim >= 0
    assert _min_points >= 2
    assert _eps >= 0.0
    assert start >= 0
    assert dims_at_once > 0
    assert pascal_tri
    assert len(pascal_tri) > 0
    assert len(_l_labels) > 0

    points = db.get_next_dim(dim, start_from_point=start, max_rows=dims_at_once)
    max_points = len(points)
    last_point = -1
    sum_dimensions = 0
    # all_cs = list() # only for debugging
    core_set = list()
    next_point = 0

    for cur_point in range(max_points - 1):
        tm.time_add(nr=2)
        next_point = max(next_point, cur_point)
        while next_point < max_points and (points[next_point][1] - points[cur_point][1] <= _eps):
            core_set.append(points[next_point][0])
            next_point += 1
        tm.time_add(nr=2)
        if len(core_set) > 0:
            # save the id of the last point that was considered as a candidate for current CS
            if last_point != core_set[-1]:
                # compute DUs only if there are enough candidates
                if len(core_set) >= _min_points:
                    if prune_core_sets:
                        cs_pruned = prune_cs(cs=core_set, boundary_low=boundary_low, boundary_high=boundary_high)
                    tm.time_add(nr=3)
                    # all_cs.append(core_set)  # only for debugging
                    if prune_core_sets:
                        if last_point not in cs_pruned or (cs_pruned.index(last_point) < _min_points - 1):
                            dus = create_dense_units_colexicographic_as_list(cs=cs_pruned, r=_min_points,
                                                                             pascal_tri=pascal_tri)
                        else:
                            dus = get_dense_units_pivot(cs=cs_pruned, pivot=cs_pruned.index(last_point),
                                                        min_points=_min_points, pascal_tri=pascal_tri)
                    else:
                        if last_point not in core_set or (core_set.index(last_point) < _min_points - 1):
                            dus = create_dense_units_colexicographic_as_list(cs=core_set, r=_min_points,
                                                                             pascal_tri=pascal_tri)
                        else:
                            dus = get_dense_units_pivot(cs=core_set, pivot=core_set.index(last_point),
                                                        min_points=_min_points, pascal_tri=pascal_tri)
                    tm.time_add(nr=3)
                    if just_calculate:
                        sum_dimensions += len(dus)
                    else:
                        tm.time_add(nr=4)
                        for du in dus:
                            signature = sum(_l_labels[s] for s in du)
                            if not has_slices or boundary_low <= signature < boundary_high:
                                added, collisions = SubscaleDict.add_to_dict_same_signatures2(dense_unit_as_indices=du,
                                                                                              signature=signature,
                                                                                              dimension=dim)

                                sum_dimensions += int(added) + collisions
                        tm.time_add(nr=4)
                last_point = core_set[-1]
        # Remove the first element of the set to move to the density chunk starting with next element
        core_set = core_set[1:]
    return sum_dimensions


def pivot_creation2(db, dim, start, dims_at_once,
                    pascal_tri, boundary_low, boundary_high,
                    just_calculate=False, has_slices=False):
    # abort program if invalid arguments
    assert db
    assert dim >= 0
    assert _min_points >= 2
    assert _eps >= 0.0
    assert start >= 0
    assert dims_at_once > 0
    assert pascal_tri
    assert len(pascal_tri) > 0
    assert len(_l_labels) > 0

    points = db.get_next_dim(dim, start_from_point=start, max_rows=dims_at_once)
    max_points = len(points)
    last_point = -1
    sum_dimensions = 0
    # all_cs = list() # only for debugging
    core_set = list()
    next_point = 0

    for cur_point in range(max_points - 1):
        tm.time_add(nr=2)
        next_point = max(next_point, cur_point)
        while next_point < max_points and (points[next_point][1] - points[cur_point][1] <= _eps):
            core_set.append(points[next_point][0])
            next_point += 1
        tm.time_add(nr=2)
        if len(core_set) > 0:
            # save the id of the last point that was considered as a candidate for current CS
            if last_point != core_set[-1]:
                # compute DUs only if there are enough candidates
                if len(core_set) >= _min_points:
                    cs = prune_cs(cs=core_set, boundary_low=boundary_low, boundary_high=boundary_high)
                    tm.time_add(nr=3)
                    # all_cs.append(core_set)  # only for debugging
                    if last_point not in cs or (core_set.index(last_point) < _min_points - 1):
                        dus = create_dense_units_colexicographic_as_list(cs=cs, r=_min_points,
                                                                         pascal_tri=pascal_tri)
                    else:
                        dus = get_dense_units_pivot(cs=cs, pivot=core_set.index(last_point),
                                                    min_points=_min_points, pascal_tri=pascal_tri)
                    tm.time_add(nr=3)
                    if just_calculate:
                        sum_dimensions += len(dus)
                    else:
                        tm.time_add(nr=4)
                        for du in dus:
                            signature = sum(_l_labels[s] for s in du)
                            if not has_slices or boundary_low <= signature < boundary_high:
                                added, collisions = SubscaleDict.add_to_dict_same_signatures2(dense_unit_as_indices=du,
                                                                                              signature=signature,
                                                                                              dimension=dim)

                                sum_dimensions += int(added) + collisions
                        tm.time_add(nr=4)
                last_point = core_set[-1]
        # Remove the first element of the set to move to the density chunk starting with next element
        core_set = core_set[1:]
    return sum_dimensions



def prune_cs(cs, boundary_low, boundary_high):
    """
    Removes points that are below boundary_low or above boundary_high
    :param cs: Core Set to prune
    :param boundary_low: lower boundary
    :param boundary_high: higher boundary
    :return: pruned Core Set
    """
    len_cs = len(cs)
    assert len_cs >= _min_points, "len_cs {}, min_points {}".format(len_cs, _min_points)
    pos = _min_points
    cs_start = 0
    cs_end = pos
    while pos < len_cs:
        sig_low = sum(_l_labels[cs_start:pos])
        if sig_low < boundary_low:
            cs_start += 1

        # sig_high = sum(_l_labels[pos-_min_points:pos])
        sig_high = sum(_l_labels[0:_min_points - 1]) + _l_labels[pos - 1]

        if sig_high < boundary_high:
            cs_end += 1
            pos += 1
        else:  # abort loop
            pos = len_cs

    if (cs_end - cs_start) >= _min_points:
        return cs[cs_start:cs_end]
    else:
        return list()



def get_dense_units_pivot(cs, pivot, min_points, pascal_tri):
    # partition the list in two parts based on pivot
    cs_left = cs[:pivot + 1]
    cs_right = cs[pivot + 1:]
    select = -1
    all_dus = list()
    if len(cs_right) >= min_points:
        all_dus.extend(create_dense_units_colexicographic_as_list(cs_right, min_points, pascal_tri))
        select = min_points - 1
    else:
        select = len(cs_right)
    count = 1
    while count <= select:
        left_dus = create_dense_units_colexicographic_as_list(cs_left, min_points - count, pascal_tri)
        right_dus = create_dense_units_colexicographic_as_list(cs_right, count, pascal_tri)
        for l_du in left_dus:
            for r_du in right_dus:
                all_dus.append(l_du + r_du)
        count += 1
    return all_dus



def find_comb_smaller_n(N, k):
    """
    Finds the highest number that is smaler than N and finds the respective n
    :param N: int
    Maximal number
    :param k: int
    Size of subsets from N
    :return: int, int
    first value: the number of combinations that are smaller than N
    second value: the last n for which the respective number combinations is smaller than N
    """
    assert N
    assert k
    n = k - 1
    while math.comb(n, k) <= N:
        n += 1
    return math.comb(n - 1, k), n - 1



def find_comb_smaller_n_pscal_triangle(N, k, pascal_tri):
    """
        Finds the highest number that is smaler than N and finds the respective n
        :param N: int
        Maximal number
        :param k: int
        Size of subsets from N
        :param pascal_tri: int
        pascal_triangle
        :return: int, int
        first value: the number of combinations that are smaller than N
        second value: the last n for which the respective number combinations is smaller than N
        """
    assert N
    assert k
    assert pascal_tri
    start = 0
    end = len(pascal_tri[k - 1])
    while True:
        c = int(end + ((start - end) / 2))
        if pascal_tri[k - 1][c] <= N:
            start = c + 1
            if pascal_tri[k - 1][start] > N:
                break
        else:
            end = c - 1

    return pascal_tri[k - 1][c], c



def combinadic(N, k, pascal_tri):
    """
    Finds a k-combinaiton for N
    https://en.wikipedia.org/wiki/Combinatorial_number_system

    :param N: int
     Number that has to be created with combinadics
    :param k: int
     Size of subsets from N
    :return: [int]
    List of the top value from binomial coefficients
    """
    assert N
    assert k
    assert pascal_tri
    res_combinadics = list()
    # subtract from N k-element sized subsets
    while N > 0:
        r, c = find_comb_smaller_n_pscal_triangle(N, k, pascal_tri)
        # k is not required in the calculation, so keep only the c
        res_combinadics.append(c)
        N -= r
        k -= 1
    if k == 2:
        res_combinadics.append(1)
        k -= 1
    if k == 1:
        res_combinadics.append(0)
    return res_combinadics

"""
test_signatures = typed.Dict()
test_signatures[123] = ([1, 2], [1])

test_subspaces = typed.Dict()
test_subspaces['123'] = [1, 2]


@jitclass([('signatures', typeof(test_signatures)),
           ('subspaces', typeof(test_subspaces))])
"""
class SubscaleDict:
    lock = RLock()

    # dimensions with same dense units
    # key: signature as int
    # value: [denseUnit, [dim_d_i, dim_d_j, ...]]
    signatures = dict()

    # a = (dict(), dict())
    # d1, d2 = a

    # points (indices of points) in common subspaces
    # key: '[dim_i, dim_j, ...]' as string
    # value: [P_i, P_j, ...]
    subspaces = dict()

    @staticmethod
    def add_to_dict_same_signatures(dense_units_as_indices, signatures, dimension,
                                    signature= 0):
        """
        Creates new dictionary with
        # key: signature as int
        # value: [denseUnit, [dim_d_i, dim_d_j, ...]]

        :param dense_units_as_indices: [int]
        List of dense units for a specified dimension d [du_d_i, du_d_(j+1), ..., du_i_(j+n)]

        :param signatures: [int]
        List of signatures for every point [Sig_x, Sig_y, ...]

        :param dimension: int
        dimension d which is to be added to the respective signature

        :return added: int
        amount of currently added dimensions in this function call
        """
        added = 0
        for du in dense_units_as_indices:
            # calculate sum of dense unit
            if signature == 0:
                for inx in du:
                    signature += signatures[inx]
            # SubscaleDict.signatures[sum_sig][du].update({dimension})
            # check for contained sum of signatures for a dense unit in dict
            # if existent, add current dimension to dimension list

            with SubscaleDict.lock:
                if signature not in SubscaleDict.signatures:
                    SubscaleDict.signatures[signature] = (du, [dimension])
                    added += 1
                else:
                    dims = SubscaleDict.signatures[signature][1]
                    if dimension not in dims:
                        dims.append(dimension)
                        dims.sort()
                        added += 1
        return added

    @staticmethod
    def add_to_dict_same_signatures2(dense_unit_as_indices, dimension, signature=0):
        """
        Creates new dictionary with
        # key: signature as int
        # value: [denseUnit, [dim_d_i, dim_d_j, ...]]

        :param dense_unit_as_indices: [int]
        List of dense units for a specified dimension d [du_d_i, du_d_(j+1), ..., du_i_(j+n)]

        :param signatures: [int]
        List of signatures for every point [Sig_x, Sig_y, ...]

        :param dimension: int
        dimension d which is to be added to the respective signature

        :param signature: int
        precalculated sum of the signatures for the DU
        """
        assert signature > 0
        collisions = 0
        added = False

        # SubscaleDict.signatures[sum_sig][du].update({dimension})
        # check for contained sum of signatures for a dense unit in dict
        # if existent, add current dimension to dimension list

        # with SubscaleDict.lock:
        if signature not in SubscaleDict.signatures:
            SubscaleDict.signatures[signature] = (dense_unit_as_indices, [dimension])
            added = True
        else:
            # get dimensions and add if necessary current dimension
            dims = SubscaleDict.signatures[signature][1]
            if dimension not in dims:
                dims.append(dimension)
                dims.sort()
                # create new entry for support of concurrent execution
                # otherwise dims inside dictionary will not be updated
                SubscaleDict.signatures[signature] = (dense_unit_as_indices, dims)
                collisions += 1
        return added, collisions

    @staticmethod
    def add_to_dict_same_subspaces():
        """
        Creates new dictionary with
        # key: [dim_i, dim_j, ...] as tuple
        # value: [P_i, P_j, ...]

        """
        for sum_sig in SubscaleDict.signatures:
            sig_entry = SubscaleDict.signatures[sum_sig]
            dense_unit_points = set(sig_entry[0])
            subspace = tuple(sig_entry[1])

            if subspace not in SubscaleDict.subspaces:
                SubscaleDict.subspaces[subspace] = dense_unit_points
            else:
                SubscaleDict.subspaces[subspace].update(dense_unit_points)
        return SubscaleDict.subspaces

    @staticmethod
    def remove_subspaces(n):
        """
        Removes entries with number unions of dense units smaller than n
        :param n: int
         amount of dense unit unions
        """
        # there has to be a second dictionary because removing items while iterating gives a
        # RuntimeError: dictionary changed size during iteration
        dict_filtered = dict()
        for subspace in SubscaleDict.subspaces:
            number_of_du_joins = SubscaleDict.subspaces[subspace][1]
            if number_of_du_joins > n:
                dict_filtered[subspace] = SubscaleDict.subspaces[subspace]
        SubscaleDict.subspaces = dict_filtered

        # Doesn't matter because we still have to create a second dictionary before the first gets overwritten?
        # SubscaleDict.subspaces = dict(filter(lambda x, y=n: x[1] > y, SubscaleDict.subspaces))


# does not function properly yet

def slice_boundary_calculator(min_signature, max_signature, db, num_dim, eps, rows, split_fac, pascal_tri):
    sum_of_DUs = 0
    slice_expanse = math.floor(max_signature - min_signature / float(split_fac))

    boundary_low = min_signature + slice_expanse * int(math.ceil(split_fac / 2.0))
    boundary_high = min_signature + slice_expanse * (int(math.ceil(split_fac / 2.0)) + 1)
    for i in range(num_dim):
        """
        boundary_low = min_signature + i * slice_expanse
        boundary_high = boundary_low + slice_expanse
        """
        sum_of_DUs += pivot_creation(db=db, dim=i, start=0,
                                     dims_at_once=rows, pascal_tri=pascal_tri,
                                     boundary_low=boundary_low, boundary_high=boundary_high,
                                     just_calculate=True)

    fdf.FourthDegreeFunction(min_signature, max_signature,
                             ((min_signature + max_signature) / 2.0), sum_of_DUs)

    slice_boundaries = list()
    for i in range(split_fac + 1):
        if i == 0:
            slice_boundaries.append(min_signature)
        elif i < split_fac:
            integral_primitive = fdf.getPrimitiveIntegralValueOf(slice_boundaries[i - 1])
            integral_definite = fdf.calculateDefiniteIntegral(min_signature, max_signature) / split_fac
            slice_boundaries.append(int(math.ceil(
                fdf.calculateRealZeroOfPrimitiveIntegralByHalley(-
                                                                 (integral_primitive + integral_definite),
                                                                 slice_boundaries[i - 1] + 2 * abs(
                                                                     (max_signature - min_signature) / split_fac * 2),
                                                                 eps))))
        else:
            slice_boundaries.append(max_signature)
    return slice_boundaries



def partition_pivot_execution(dims_at_once, io, n_processes, just_calculate=_just_calculate):
    sum_dus = 0
    init_tables()
    tm.start(1)
    for p in range(n_processes):
        for split_num in range(_split_fac):
            print("split nr: {}/{}".format(split_num, _split_fac - 1))
            for dim in range(io.get_dimensions()):
                # tm.start(2)
                boundary_low = _l_boundaries[split_num]
                boundary_high = _l_boundaries[split_num + 1]
                # print(dim)
                sum_dus += pivot_creation(db=io, dim=dim, start=p * dims_at_once,
                                          dims_at_once=dims_at_once,
                                          pascal_tri=HelperFunctions.PascalTriangle.get_pascal_triangle(),
                                          boundary_low=boundary_low, boundary_high=boundary_high,
                                          has_slices=_has_slices, just_calculate=just_calculate)
                # for profiling or debugging:
                # amount_of_dus_in_slices.append(sum_dus)
                # msg_sig = "Signatures with pivot in slice {} in dim {}".format(split_num, dim)
                # tm.time_display(nr=2, msg=msg_sig, len_of_list=sum_dus)

            # saves slices
            if _has_slices:
                SubscaleDict.add_to_dict_same_subspaces()
                if _is_saved_as_slices:
                    io.save_slice(SubscaleDict.subspaces, slice_nr=split_num)
                else:
                    io.save_subspaces_to_csv(SubscaleDict.subspaces, is_append=True)
                init_tables()

    tm.time_display(nr=1, msg="Signatures with pivot created", len_of_list=sum_dus)

    tm.sum_times(2)
    tm.time_display(nr=2, msg="Core Sets created", dont_set_end_time=True)
    tm.sum_times(3)

    tm.time_display(nr=3, msg="DUs", len_of_list=sum_dus, dont_set_end_time=True)
    tm.sum_times(4)
    tm.time_display(nr=4, msg="Signatures added", len_of_list=len(SubscaleDict.signatures), dont_set_end_time=True)


def partition_non_pivot_execution(n_processes, dims_at_once, all_core_sets_as_indices, labels, io):
    # thread_args = list()
    sum_cs = 0
    sum_dus = 0
    init_tables()
    for p in range(n_processes):
        if _has_slices:
            for split_num in range(_split_fac):
                boundary_low = _l_boundaries[split_num]
                boundary_high = _l_boundaries[split_num + 1]
                """
                thread_args = [p * dims_at_once, dims_at_once, all_core_sets_as_indices,
                               labels, pascal_tri,
                               boundary_low,
                               boundary_high,
                               just_calculate]
                """

                sum_cs += createDU_and_sig_map_no_pivot(start_dim=p * dims_at_once,
                                                        n_threads=dims_at_once,
                                                        core_sets=all_core_sets_as_indices, labels=labels,
                                                        pascal_tri=HelperFunctions.PascalTriangle.get_pascal_triangle(),
                                                        min_points=_min_points,
                                                        boundary_low=boundary_low,
                                                        boundary_high=boundary_high,
                                                        just_calculate=_just_calculate)
                # experimental: saves slices
                SubscaleDict.add_to_dict_same_subspaces()
                if _is_saved_as_slices:
                    io.save_slice(SubscaleDict.subspaces, slice_nr=split_num)
                else:
                    io.save_subspaces_to_csv(SubscaleDict.subspaces, is_append=True)
                init_tables()
                """
                SubscaleDict.add_to_dict_same_subspaces()
                io.save_subspaces_to_csv(SubscaleDict.subspaces)
                init_tables()
                """
        else:
            # with multiprocessing.Pool(processes=n_processes) as p:
            #    p.map(createDU_and_sig_map, thread_args)
            """
            thread_args = [p * dims_at_once, dims_at_once, all_core_sets_as_indices, min_points,
                           labels, pascal_tri, transposed,
                           0,
                           slice_size_sig,
                           has_slices]
            """
            sum_cs += createDU_and_sig_map_no_pivot(start_dim=p * dims_at_once, n_threads=dims_at_once,
                                                    core_sets=all_core_sets_as_indices, labels=labels,
                                                    pascal_tri=HelperFunctions.PascalTriangle.get_pascal_triangle(),
                                                    min_points=_min_points,
                                                    boundary_low=_minSig, boundary_high=_maxSig,
                                                    just_calculate=_just_calculate)
    tm.time_display(nr=1, msg="Signatures-Table withOUT pivot created", len_of_list=sum_dus)
    tm.sum_times(2)
    tm.sum_times(3)
    tm.time_display(nr=2, msg="DUs", dont_set_end_time=True)
    summed_cs = sum([len(cs) for cs in all_core_sets_as_indices])
    return sum_dus



def slice_boundaries_calculation(io):
    _l_boundaries = list()
    if _use_evenly_distributed_slices:
        _l_boundaries = slice_boundary_calculator(min_signature=_minSig, max_signature=_maxSig,
                                                  db=io,
                                                  num_dim=io.get_dimensions(),
                                                  eps=_eps, rows=io.get_size_rows(),
                                                  split_fac=_split_fac,
                                                  pascal_tri=HelperFunctions.PascalTriangle.size)
        tm.time_display(nr=1, msg="Boundaries of slices calculated", len_of_list=len(_l_boundaries))
    else:
        # size of each slice
        slice_size_sig = math.ceil((_maxSig - _minSig) / _split_fac)
        for split_num in range(_split_fac):
            _l_boundaries.append(_minSig + split_num * slice_size_sig)
        _l_boundaries.append(_maxSig)
    return _l_boundaries


def init_tables():
    if _is_multiprocessing:
        # multiprocessing led to extreme, exponential decrease in execution speed
        mpm = multiprocessing.Manager()
        SubscaleDict.subspaces = mpm.dict()
        SubscaleDict.signatures = mpm.dict()
    else:
        SubscaleDict.subspaces = dict()
        SubscaleDict.signatures = dict()



def refine_subspaces(io):
    tm.start(1)
    # check if slices or subspaces are saved
    if _is_saved_as_slices:
        path_of_choice = io.get_file_path_slice()
    else:
        path_of_choice = io.get_file_path_results()
        init_tables()

    filenames_subspaces = io.get_subspace_filenames(path_of_choice)
    if len(filenames_subspaces) > 0:
        full_file_paths = io.get_full_file_paths(path_of_choice, filenames_subspaces)
        for file_path in full_file_paths:
            file_subspaces_dict = io.load_subspace(file_path)
            for subspace, pointIDs in file_subspaces_dict.items():
                if subspace not in SubscaleDict.subspaces:
                    SubscaleDict.subspaces[subspace] = set(pointIDs)
                else:
                    SubscaleDict.subspaces[subspace].update(set(pointIDs))
            if not _is_saved_as_slices:
                io.save_subspaces_to_csv(SubscaleDict.subspaces, is_append=False)
                init_tables()
    if _is_saved_as_slices:
        io.save_subspaces_to_csv(SubscaleDict.subspaces, is_append=True)
    tm.time_display(nr=1, msg="Subspaces refined")


# The dims_at_once variable has no connection to the boundaries.
# it is set to process only a part of available dimensions at once
# it is the fraction of the number of dimensions
# not implemented yet, so dims_at_once is set to the number of dimensions, dependent on the transposed argument
def calculate_dimensions_at_once(io):
    if _is_pivot:
        if io.is_transposed():
            dims_at_once = int(io.get_dimensions() / _n_processes)
        else:
            dims_at_once = int(io.get_size_rows() / _n_processes)
    else:
        if io.is_transposed():
            dims_at_once = int(io.get_size_rows() / _n_processes)
        else:
            dims_at_once = int(io.get_dimensions() / _n_processes)
    # make dims_at_once dependent on number of launched calculation processes
    dims_at_once = int(dims_at_once / _n_processes)
    return dims_at_once
