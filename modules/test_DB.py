import unittest
from pathlib import Path
from unittest import TestCase

from modules import IO as Dbase


class TestDB(unittest.TestCase):
    def test_get_next_row(self):
        filepath = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test_transposed.csv"
        db = Dbase.DB(filepath)
        row = db.get_next_row()
        self.assertEqual(row, [(0, 0.11), (1, 0.21), (2, 0.31)])

    def test_get_current_row(self):
        filepath = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test_transposed.csv"
        db = Dbase.DB(filepath)
        row = db.get_next_row()
        self.assertEqual(db.get_current_row(), [(0, 0.11), (1, 0.21), (2, 0.31)])

    def test_load_df(self):
        filepath = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test.csv"
        db = Dbase.DB(filepath)
        db.load_df()
        df = db.get_df()
        self.assertTrue(df is not None)

    def test_save_to_csv(self):
        filepath = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test.csv"
        filepath_results = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test_results\2.csv"
        db = Dbase.DB(filepath)
        test_dict = {(1, 2): {0, 1, 2}, (1,): {2, 3, 4, 5, 6}, (2,): {3, 4, 5, 6, 7}}
        db.save_subspaces_to_csv(test_dict)
        path = Path(filepath_results)
        assert path.is_file()

    def test_create_random_data_set(self):
        filepath = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\test.csv"
        db = Dbase.DB(filepath)
        db.create_random_data_set(20, 10)
        path = Path(filepath)
        assert path.is_file()


class Test(TestCase):
    def test_transpose(self):
        filepath_begin = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\20x10_random.csv"
        filepath_end = r"C:\Users\ataru\Desktop\hso\thesis\SUBSCALE\daten\resources\resources\20x10_random_transposed" \
                       r".csv "
        Dbase.transpose(filepath_begin)
        path = Path(filepath_end)
        assert path.is_file()


if __name__ == '__main__':
    unittest.main()
