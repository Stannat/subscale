import itertools
import json
import linecache
from pathlib import Path

import numpy as np
import pandas as pd
import os
import csv
import shutil


def load_json(config_path):
    config = None
    abs_config_path = os.path.abspath(config_path)
    with open(abs_config_path) as json_file:
        config = json.load(json_file)
    return config


class DB:
    """ DB container for file access

    """

    def __init__(self, file_path, result_path, is_transposed=False):
        self.__separator = ','
        self.__filepath = file_path
        self.__line = list()
        self.__nextRow = 1  # starts with first line
        self.__df_allPoints = None
        self.__isTransposed = is_transposed
        self.__file_path_results = result_path
        self.__file_path_dbscan = os.path.join(self.__file_path_results, 'dbscan')
        self.__file_path_slices = os.path.join(self.__file_path_results, 'slices')
        self.__whole_data_set = list()
        self.__is_whole_data_set = False
        linecache.lazycache(self.__filepath, None)
        if is_transposed:
            self.__rows = sum(1 for line in open(file_path))
            self.__cols = len(self.get_current_row())
            self.__points = self.__cols
            self.__dimensions = self.__rows
        else:
            self.__rows = sum(1 for line in open(file_path))
            self.__cols = len(self.get_current_row())
            self.__points = self.__rows
            self.__dimensions = self.__cols

    def get_size(self):
        return self.__rows, self.__cols

    def get_current_row(self):
        """        Returns current row

        :return: list[(int, float)]
        A list of float numbers
        """
        if self.__line is None or len(self.__line) == 0:
            return self.get_next_row(1)
        else:
            return self.__line

    def is_transposed(self):
        return self.__isTransposed

    def get_next_row(self, row_no=-1):
        """        Returns specified line from file
        :param row_no: int, optional
        The line to read from. If none specified, the next line from self.__line will be read

        :return: [float] or None
        A list of float numbers
        """
        # if an argument was specified, then read the specified line from file
        # otherwise the next line
        if row_no != -1:
            self.__line = linecache.getline(self.__filepath, row_no)
        else:
            self.__line = linecache.getline(self.__filepath, self.__nextRow)
        linecache.clearcache()

        if len(self.__line) > 1:
            # slit list values by comma
            self.__line = self.__line.split(self.__separator)
            # string to float conversion
            self.__line = list(map(float, self.__line))
            # make a list of 2-element tuples (int, float) where the first element
            # is the position and the second the value
            self.__line = [(num, self.__line[num]) for num in range(len(self.__line))]
            # if no argument was given, then autoincrement the position of the next line to read
            if row_no == -1:
                self.__nextRow += 1
        else:
            self.__line = None
        return self.__line

    def get_dimensions(self):
        return self.__dimensions

    def load_df(self):
        self.__df_allPoints = pd.read_csv(self.__filepath)

    def get_df(self):
        return self.__df_allPoints

    def get_filepath(self):
        return self.__filepath

    def get_file_path_results(self):
        return self.__file_path_results

    def get_file_path_slice(self):
        return self.__file_path_slices

    def get_size_rows(self):
        return self.__rows

    def delete_directory(self, path):
        path = Path(self.__file_path_results)
        if path.exists():
            shutil.rmtree(path, ignore_errors=True)
        os.makedirs(self.__file_path_results)
        os.makedirs(self.__file_path_slices)

    def load_whole_data_set(self, filename):
        self.__is_whole_data_set = True
        with open(filename, 'r') as subs_file:
            for line in subs_file:
                if len(line) > 1:
                    line = line.split(self.__separator)
                    # string to float conversion
                    line = list(map(float, line))
                    self.__whole_data_set.append(line)

    def save_subspaces_to_csv(self, clusters, is_append):
        """
        Saves clusters to files named as the number of dimensions of the containing clusters
        :param clusters: [dim_i, ... , dim_j]-[point_a, ... , point_c]
        :param is_append: false to select 'w' to create a new file, true for 'a' to append to existing one
        :return:
        """
        if is_append:
            write_mode = 'a'
        else:
            write_mode = 'w'
        lastSubspaceSize = 0
        same_sized_clusters = ""
        for subspace, points in clusters.items():
            cl_size = len(subspace)

            if lastSubspaceSize != 0 and lastSubspaceSize != cl_size:
                new_file_path = os.path.join(self.__file_path_results, "{}{}".format(str(lastSubspaceSize), '.csv'))
                with open(new_file_path, write_mode) as o_file:
                    o_file.write(same_sized_clusters)
                same_sized_clusters = ""

            key = ", ".join(str(s) for s in subspace)
            val = ", ".join(str(s) for s in points)
            same_sized_clusters += '[' + key + ']-[' + val + ']\n'
            lastSubspaceSize = cl_size

        new_file_path = os.path.join(self.__file_path_results, "{}{}".format(str(lastSubspaceSize), '.csv'))
        with open(new_file_path, write_mode) as o_file:
            o_file.write(same_sized_clusters)

    def get_subspace_filenames(self, file_path_folder):
        files = [f for f in os.listdir(file_path_folder) if os.path.isfile(os.path.join(file_path_folder, f))]
        files_subspaces = list()
        for filename in files:
            name_parts = filename.split('.')
            if name_parts[0].isnumeric() and name_parts[-1] == 'csv':
                files_subspaces.append(filename)
        return files_subspaces

    def get_full_file_paths(self, file_path_folder, files_subspaces):
        file_paths = list()
        for filename in files_subspaces:
            file_path = os.path.join(file_path_folder, filename)
            file_paths.append(file_path)
        return file_paths

    def load_all_subspaces(self, subs_dict):
        was_loading_successful = False
        if subs_dict is None or len(subs_dict) > 0 or not os.path.exists(self.__file_path_slices):
            pass
        else:
            files_subspaces = self.get_subspace_filenames(self.__file_path_slices)
            if len(files_subspaces) == 0:
                was_loading_successful = False
            else:
                for filepath in self.get_full_file_paths(self.__file_path_slices, files_subspaces):
                    self.load_subspace(filepath, subs_dict)
                was_loading_successful = True
        return was_loading_successful

    def load_subspace(self, filename):
        subs_dict = dict()
        with open(filename, 'r') as subs_file:
            for line in subs_file:
                line = line.strip('\n')
                key, val = line.split('-')
                key = key.strip('[]')
                val = val.strip('[]')
                key = tuple(int(x) for x in key.split(','))
                val = [int(x) for x in val.split(',')]
                subs_dict[key] = val
        return subs_dict

    def get_slice_files(self):
        if len(os.listdir(self.__file_path_slices)) > 0:
            filenames = self.get_subspace_filenames(self.__file_path_slices)
            if len(filenames) == 0:
                raise Exception("directory contains no slices!\n{}".format(self.__file_path_slices))
            else:
                return filenames
        else:
            raise Exception("directory is empty!\n{} \n No slices found.".format(self.__file_path_slices))

    # not used in this implementation, because everything is written into subspaces
    # afterwards, they are loaded and refined
    def save_slice(self, d_subspaces, slice_nr):
        out_strings = ""
        for subspace, points in d_subspaces.items():
            subspace = ", ".join(str(s) for s in subspace)
            points = ", ".join(str(s) for s in points)
            out_strings += "[{}]-[{}]\n".format(subspace, points)
            if len(out_strings) > 10000:
                slice_file_path = os.path.join(self.__file_path_slices, "{}.csv".format(slice_nr))
                # append to file because of len(out_strings)
                with open(slice_file_path, 'a') as slice_file:
                    slice_file.write(out_strings)
                out_strings = ""

    def slices_delete(self):
        path = Path(self.__file_path_slices)
        if path.exists():
            shutil.rmtree(path, ignore_errors=True)
        os.makedirs(self.__file_path_slices)

    def dbscan_remove(self):
        path = Path(self.__file_path_dbscan)
        if path.exists():
            shutil.rmtree(path, ignore_errors=True)
        os.makedirs(self.__file_path_dbscan)

    def dbscan_save(self, subspace, points, labels):
        new_file_path = os.path.join(self.__file_path_dbscan, str(len(subspace)), '.csv')

        with open(new_file_path, 'a') as o_file:
            for i in range(len(labels)):
                o_file.write(str(subspace) + ', ' + str(points[i]) + ', ' + str(labels[i]) + '\n')

    def dbscan_save_all(self, dbscan_dict):
        """
        writes multiple records for Subspace, Points, clusterNr for same subspace
        :param dbscan_dict:
        :return:
        """
        file_path_folder = self.__file_path_dbscan
        if len(list(dbscan_dict.keys())) > 0:
            file_name = str(len(list(dbscan_dict.keys())[0]))
            new_file_path = "{}{}.csv".format(file_path_folder, file_name)
            file_input = ''
            file_input2 = list()
            for key, val in dbscan_dict.items():
                points = val[0]
                labels = val[1]
                # tm.start(2)
                for pos_label in range(len(labels)):
                    # file_input += "{}-{}, {}\n".format(list(key), points[pos_label], labels[pos_label])
                    file_input2.append("{}-{}, {}\n".format(list(key), points[pos_label], labels[pos_label]))
                    if len(file_input2) > 10000:
                        # tm.time_display(nr=2, msg="String length={}".format(len(file_input)))
                        # tm.start(3)
                        # self.dbscan_write(new_file_path, file_input, key)
                        self.dbscan_write_lines(new_file_path, file_input2, key)
                        # tm.time_display(nr=3, msg="String written={}".format(key))
                        # tm.start(2)
                        # file_input = ''
                        file_input2 = list()
            # tm.time_display(nr=2, msg="String length={}".format(len(file_input)))
            if len(file_input2) > 0:
                # tm.start(3)
                # self.dbscan_write(new_file_path, file_input, key)
                self.dbscan_write_lines(new_file_path, file_input2, key)
                # tm.time_display(nr=3, msg="String written={}".format(key))

    def dbscan_write(self, new_file_path, file_input):
        # tm.start(2)
        with open(new_file_path, 'a') as o_file:
            o_file.write(file_input)
        # tm.time_display(nr=2, msg="Output={}".format(list(key)))

    def dbscan_write_lines(self, new_file_path, file_input, key):
        # tm.start(2)
        with open(new_file_path, 'a') as o_file:
            o_file.writelines(file_input)
        # tm.time_display(nr=2, msg="Output={}".format(list(key)))

    def dbscan_count_out(self, maximal_clusters_stat):
        file_path = self.__filepath.split('.')[:-1][0]
        file_path_folder = file_path + '_results\\dbscan\\'
        new_file_path = file_path_folder + 'count_clusters' + '.txt'
        ordered = dict(sorted(maximal_clusters_stat.items(), key=lambda t: len(t[0])))
        with open(new_file_path, 'a') as o_file:
            for key, val in ordered.items():
                o_file.write("{}-{}\n".format(list(key), val))

    def create_random_data_set(self, n, dim):
        random_table = np.random.rand(n, dim)
        name = str(n) + 'x' + str(dim) + '_random.csv'
        file_path = '\\'.join(self.__filepath.split('\\')[:-1]) + '\\' + name
        if os.path.exists(file_path):
            os.remove(file_path)
        for line in random_table:
            with open(file_path, 'a', newline='') as o_file:
                with o_file:
                    write = csv.writer(o_file)
                    write.writerow(line)

    def get_next_dim(self, dim, start_from_point=-1, max_rows=0):
        """
        Loads and enumerates points from the required dimension
        :param dim: dimension to load points from
        :param start_from_point: only for not transposed data, defines from which row  we should start to read
        :param max_rows: number of rows that have to be read from start_from_point
        :return: list of tuples where first element is the id and the second element is the float value
        """
        if self.__isTransposed:
            return self.get_next_row(dim + 1)
        else:
            assert start_from_point >= 0
            # make sure that all rows are loaded if parameter is not specified
            if max_rows == 0:
                max_rows = self.__rows

            if self.__is_whole_data_set:
                points = list()
                for i in range(self.__points):
                    points.append(self.__whole_data_set[i][dim])
            else:
                points = np.loadtxt(self.__filepath, skiprows=start_from_point, delimiter=self.__separator, usecols=dim,
                                    max_rows=max_rows, dtype=float)
            points = [(_id, points[_id]) for _id in range(len(points))]
            points = sorted(points, key=lambda entry: entry[1])
            return points

    def get_dim_with_pos(self, dim):
        try:
            if self.__is_whole_data_set:
                col = list()
                for i in range(self.__points):
                    col.append(self.__whole_data_set[i][dim])
            else:
                col = np.loadtxt(self.__filepath, skiprows=0, delimiter=self.__separator, usecols=dim, max_rows=None,
                                 dtype=float)
            col = [(num, col[num]) for num in range(len(col))]
        except IndexError:
            col = None
        return col

    def get_dim_with_pos2(self, dim):
        if self.__isTransposed:
            return self.get_next_row(row_no=dim)
        else:
            return self.get_dim_with_pos(dim)

    def get_point_with_pos(self, pos: int):
        if self.__isTransposed:
            return self.get_dim_with_pos(pos)
        else:
            return self.get_next_row(row_no=pos + 1)


def transpose(filepath):
    """     Reads csv-file and writes its values in a transposed manner.
    Output file is filepath_transposed.csv

    :param filepath: str
    The file to be opened, transposed and written back to a new file
    """
    separator = ","
    new_file_path = ''.join(filepath.split('.')[:-1]) + '_transposed.csv'
    with open(filepath) as r, open(new_file_path, "w") as w:
        try:
            for column_index in itertools.count():
                r.seek(0)
                col = [line.strip('\n').split(separator)[column_index] for line in r]
                w.write(separator.join(col) + "\n")
        except IndexError:
            pass
