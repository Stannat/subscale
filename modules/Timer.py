import datetime
import operator
import functools

timers = dict()
timestamps = dict()
saved_timestamps = ""
path = ""


def init(filepath):
    global path
    path = filepath


def out():
    with open(path, 'w') as o_file:
        o_file.write(saved_timestamps)


def start(i):
    timers[i] = {"start": datetime.datetime.now(), "end": datetime.datetime.now()}


def get_time_interval(i):
    assert i in timers
    timers[i]["end"] = datetime.datetime.now()
    """
    t_start = timedelta(minutes=timers[i]["start"].minute, seconds=timers[i]["start"].second,
                        microseconds=timers[i]["start"].microsecond)
    t_end = timedelta(minutes=timers[i]["end"].minute, seconds=timers[i]["end"].second,
                      microseconds=timers[i]["end"].microsecond)
    time_dif = t_end - t_start
    """
    time_dif = timers[i]["end"] - timers[i]["start"]
    return time_dif


def time_add(nr):
    if nr not in timestamps:
        timestamps[nr] = list()
    timestamps[nr].append(datetime.datetime.now())


def sum_times(nr):
    """
    sums all times in timers of nr and sets timers[nr] start and end entries
    :param nr: number of timer
    :return: the summed time
    """
    summed_time = 0
    if nr in timestamps:
        max_dif = datetime.datetime.now() - datetime.datetime.now()
        l_times_nr = timestamps[nr]
        l_summed = list()
        time_start = None
        time_end = None
        if len(l_times_nr) > 0:
            # add start time to timers dict
            if nr not in timers:
                timers[nr] = {"start": l_times_nr[0]}
            else:
                timers[nr]["start"] = l_times_nr[0]
            for time_nr in range(len(l_times_nr)):
                if time_nr % 2 == 0:
                    time_start = l_times_nr[time_nr]
                else:
                    time_end = l_times_nr[time_nr]
                    time_dif = time_end - time_start
                    if time_dif > max_dif:
                        max_dif = time_dif
                    l_summed.append(time_dif)
            summed_time = functools.reduce(operator.add, l_summed)
            timers[nr]["end"] = timers[nr]["start"] + summed_time
            timestamps[nr] = list()
    return summed_time


def time_display(nr, msg, pos=-1, len_of_list=-1, dont_set_end_time=False):
    global saved_timestamps
    if nr in timers:
        if dont_set_end_time:
            time_dif = str(timers[nr]["end"] - timers[nr]["start"])[:-3]
        else:
            time_dif = str(get_time_interval(nr))[:-3]

        if pos != -1:
            msg = msg + ", Dimension: " + str(pos)
        if len_of_list != -1:
            msg = msg + ", Amount of elements: " + f"{len_of_list:,}"
        """
        sec = time_dif.seconds
        ms = time_dif.microseconds
        output_msg = msg + "; Time for function: %s.%s [s]" % (time_dif.seconds, str(time_dif.microseconds)[:3])
        """
        output_msg = msg + "; Time for function: {}".format(time_dif)
        print(output_msg)
        saved_timestamps += output_msg + '\n'
