A: float = 0.0
B: float = 0.0
C: float = 0.0
D: float = 0.0
E: float = 0.0

def FourthDegreeFunction(n1: float, n2: float, h: float, fh: float):
    """
    :param n1: The position of the first real zero
    :param n2: The position of the second real zero
    :param h: The position of the maximal turning point
    :param fh: The value of the maximal turning point
    """
    global A, B, C, D, E
    A = fh / ((h - n1) ** 4.0)
    B = -(2.0 * n1 + 2.0 * n2)
    C = (n1 ** 2.0) + 4.0 * n1 * n2 + (n2 ** 2.0)
    D = -(2.0 * n1 * (n2 ** 2.0) + 2.0 * (n1 ** 2.0) * n2)
    E = (n1 ** 2.0) * (n2 ** 2.0)
    a = 3

def getValueOf(x: float):
    return A * ((x ** 4) + B * (x ** 3) + C * (x ** 2) + D * x + E)


def getFirstDerivationValueOf(x: float):
    return A * (4 * (x ** 3) + B * 3 * (x ** 2) + C * 2 * x + D)


def getSecondDerivationValueOf(x: float):
    return A * (12 * (x ** 2) + B * 6 * x + C * 2)


def getPrimitiveIntegralValueOf(x: float):
    first_sum = (x ** 5) / 5
    second_sum = (B * (x ** 4)) / 4
    third_sum = (C * (x ** 3)) / 3
    fourth_sum = (D * (x ** 2)) / 2
    last_sum = E * x
    return A * ((x ** 5) / 5 + (B * (x ** 4)) / 4 + (C * (x ** 3)) / 3 + (D * (x ** 2)) / 2 + E * x)


def calculateDefiniteIntegral(lower_boundary: float, higher_boundary: float):
    return getPrimitiveIntegralValueOf(higher_boundary) - getPrimitiveIntegralValueOf(lower_boundary)


def calculateRealZeroByNewton(x: float, epsilon: float):
    i = 0
    # Calculate real zero
    while abs(getValueOf(x)) > epsilon and i < 20:
        x = x - (getValueOf(x) / getFirstDerivationValueOf(x))
        i += 1

    # Return real zero
    return x


def calculateRealZeroByHalley(x: float, epsilon: float):
    i = 0
    # Calculate real zero
    while abs(getValueOf(x)) > epsilon and i < 20:
        x = x - ((2 * getValueOf(x) * getFirstDerivationValueOf(x)) / (
                    2 * (getFirstDerivationValueOf(x) ** 2) - getValueOf(x) * getSecondDerivationValueOf(x)))
        i += 1
    # Return real zero
    return x


def calculateRealZeroOfPrimitiveIntegralByNewton(f: float, x: float, epsilon: float):
    i = 0
    # Calculate real zero
    while abs(getPrimitiveIntegralValueOf(x) + f) > epsilon and i < 20:
        x = x - ((getPrimitiveIntegralValueOf(x) + f) / getValueOf(x))
        i += 1

    # Return real zero
    return x


def calculateRealZeroOfPrimitiveIntegralByHalley(f: float, x: float, epsilon: float):
    i = 0
    # Calculate real zero
    while abs(getPrimitiveIntegralValueOf(x) + f) > epsilon and i < 20:
        x = x - ((2 * (getPrimitiveIntegralValueOf(x) + f) * getValueOf(x)) / (
                    2 * (getValueOf(x) ** 2) - (getPrimitiveIntegralValueOf(x) + f) * getFirstDerivationValueOf(x)));
        i += 1
    # Return real zero
    return x
