from unittest import TestCase
import modules.SUBSCALE as SUBSCALE
import modules.HelperFunctions as HF

class Test(TestCase):
    # calculate pascal triangle for further use
    HF.PascalTriangle.create_pascal_triangle(5, 100)

    def test_init_seed(self):
        init_seed_expected = [-1, 3, 4]
        init_seed_is = SUBSCALE.init_seed(c=5, r=3)
        self.assertEqual(init_seed_expected, init_seed_is)

    def test_create_subsets_lex(self):
        cs = [10, 20, 30, 40, 50]
        subsets_expected = [[10, 20, 30], [10, 20, 40], [10, 20, 50], [10, 30, 40], [10, 30, 50], [10, 40, 50],
                            [20, 30, 40], [20, 30, 50], [20, 40, 50], [30, 40, 50]]
        subsets_is = SUBSCALE.create_subsets_lex(cs=cs, r=3)
        self.assertEqual(subsets_expected, subsets_is)

    def test_subsequent_colex_index_combination(self):
        start_sequence = [5, 3, 2]
        next_sequence_expected = [5, 4, 1]
        next_sequence_is, changed_index = SUBSCALE.subsequent_colex_index_combination(C=start_sequence, k=3)
        self.assertEqual(next_sequence_expected, next_sequence_is)
        # the last index is at 1, because 3 in [5, 3, 2] was changed to 4
        self.assertEqual(changed_index, 1)

    def test_subsequent_colex_index_combination_2(self):
        start_sequence = [2, 3, 5]
        next_sequence_expected = [0, 4, 5]
        next_sequence_is, changed_index = SUBSCALE.subsequent_colex_index_combination_2(C=start_sequence, k=3)
        self.assertEqual(next_sequence_expected, next_sequence_is)
        # the last index is at 1, because 3 in [2, 3, 5] was changed to 4
        self.assertEqual(1, changed_index)

    def test_create_dense_units_colexicographic(self):
        cs = [10, 20, 30, 40, 50]
        r = 3
        pasc_tri = HF.PascalTriangle.get_pascal_triangle()
        resulted_subsets = SUBSCALE.create_dense_units_colexicographic(cs, r, pasc_tri)
        expected_subsets = [[10, 20, 30], [10, 20, 40], [10, 30, 40], [20, 30, 40], [10, 20, 50],
                            [10, 30, 50], [20, 30, 50], [10, 40, 50], [20, 40, 50], [30, 40, 50]]
        self.assertEqual(expected_subsets, resulted_subsets)

    def test_create_dense_units_colexicographic_no_list(self):
        cs = [10, 20, 30, 40, 50]
        r = 3
        pasc_tri = HF.PascalTriangle.get_pascal_triangle()
        resulted_subsets = SUBSCALE.create_dense_units_colexicographic_as_list(cs, r, pasc_tri)
        expected_subsets = [[10, 20, 30], [10, 20, 40], [10, 30, 40], [20, 30, 40], [10, 20, 50],
                            [10, 30, 50], [20, 30, 50], [10, 40, 50], [20, 40, 50], [30, 40, 50]]
        self.assertEqual(expected_subsets, resulted_subsets)

    def test_find_comb_smaller_N(self):
        resulted_comb, resulted_n = SUBSCALE.find_comb_smaller_n(N=72, k=5)
        expected_n = 8
        expected_comb = 56
        self.assertEqual(expected_n, resulted_n)
        self.assertEqual(expected_comb, resulted_comb)

    def test_combinadic(self):
        pasc_tri = HF.PascalTriangle.get_pascal_triangle()

        resulted_seq = SUBSCALE.combinadic(N=72, k=5, pascal_tri=pasc_tri)
        expected_seq = [8, 6, 3, 1, 0]
        self.assertEqual(expected_seq, resulted_seq)

        resulted_seq = SUBSCALE.combinadic(N=6, k=3, pascal_tri=pasc_tri)
        expected_seq = [4, 2, 1]
        self.assertEqual(expected_seq, resulted_seq)

        resulted_seq = SUBSCALE.combinadic(N=7, k=4, pascal_tri=pasc_tri)
        expected_seq = [5, 3, 2, 0]
        self.assertEqual(expected_seq, resulted_seq)

        #resulted_seq = SUBSCALE.combinadic(N=5, k=2, pascal_tri=pasc_tri)
        #a = 0

    def test_create_dense_units_colexicographic_no_list2(self):
        cs = [10, 20, 30, 40]
        next_seq = [0, 1, 2]
        last_pos = -1
        r = 3
        pasc_tri = HF.PascalTriangle.get_pascal_triangle()
        resulted_du, next_seq, last_pos = SUBSCALE.create_dense_units_colexicographic_no_list2(cs, r, next_seq,
                                                                                      last_pos)
        expected_du = [10, 20, 30]
        self.assertEqual(expected_du, resulted_du)


class TestSubscaleDict(TestCase):
    def test_add_to_dict_same_signatures(self):
        SUBSCALE.SubscaleDict.signatures = dict()
        # there have to be as much signatures as points in dense units in a dimension
        signatures_all_dim = [1235, 4214, 3121, 123, 4234, 5341, 243424, 2136]  # size = 7
        dense_units_dim1 = [[0, 1, 2], [2, 3, 4, 5], [4, 5, 6]]
        expected_seq_value = ([0, 1, 2], [1])
        first_signature = sum(signatures_all_dim[:3])
        SUBSCALE.SubscaleDict.add_to_dict_same_signatures(dense_units_as_indices=dense_units_dim1,
                                                          signatures=signatures_all_dim,
                                                          dimension=1)
        resulted_seq_value = SUBSCALE.SubscaleDict.signatures[first_signature]
        self.assertEqual(expected_seq_value, resulted_seq_value)

        # check in different dimensions
        dense_units_dim2 = [[0, 1, 2], [3, 4, 5], [4, 5, 6, 7]]
        SUBSCALE.SubscaleDict.add_to_dict_same_signatures(dense_units_as_indices=dense_units_dim2,
                                                          signatures=signatures_all_dim,
                                                          dimension=2)
        resulted_seq_value = SUBSCALE.SubscaleDict.signatures[first_signature]
        expected_seq_value = ([0, 1, 2], [1, 2])
        self.assertEqual(expected_seq_value, resulted_seq_value)

    def test_add_to_dict_same_subspaces(self):
        SUBSCALE.SubscaleDict.subspaces = dict()
        SUBSCALE.SubscaleDict.add_to_dict_same_subspaces()
        expected_dict = {(1, 2): {0, 1, 2}, (1,): {2, 3, 4, 5, 6}, (2,): {3, 4, 5, 6, 7}}
        resulted_dict = SUBSCALE.SubscaleDict.subspaces
        self.assertEqual(expected_dict, resulted_dict)

    """
    def test_remove_subspaces(self):
        self.fail()
        SUBSCALE.SubscaleDict.remove_subspaces(1)
        expected_dict = {'[1]': [{2, 3, 4, 5, 6}, 2], '[2]': [{3, 4, 5, 6, 7}, 2]}
        resulted_dict = SUBSCALE.SubscaleDict.subspaces
        self.assertEqual(expected_dict, resulted_dict)
    """


class TestComb(TestCase):
    def test_calculate_pascal_triangle(self):
        HF.PascalTriangle(min_points=5, num_of_all_points=7)
        resulted_pascal_triangle = HF.PascalTriangle.create_pascal_triangle(min_points=5, num_of_all_points=7)

        expected_pascal_triangle = [[0, 1, 2, 3, 4, 5, 6],
                                    [0, 0, 1, 3, 6, 10, 15],
                                    [0, 0, 0, 1, 4, 10, 20],
                                    [0, 0, 0, 0, 1, 5, 15],
                                    [0, 0, 0, 0, 0, 1, 6]]
        self.assertEqual(expected_pascal_triangle, resulted_pascal_triangle)
